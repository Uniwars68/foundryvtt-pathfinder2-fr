# État de la traduction (bestiary-family-ability-glossary)

 * **officielle**: 62
 * **libre**: 52
 * **changé**: 37


Dernière mise à jour: 2022-04-27 10:36 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des éléments changés en VO et devant être vérifiés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[49aAeGbH5p2IJ5Fz.htm](bestiary-family-ability-glossary/49aAeGbH5p2IJ5Fz.htm)|(Kuworsys) Rapid Bombardment|(Kuworsys) Bombardement rapide|changé|
|[d6eWuFd6pw6ffvPC.htm](bestiary-family-ability-glossary/d6eWuFd6pw6ffvPC.htm)|(Melfesh Monster) Swell|(Monstre melfesh) Gonfler|changé|
|[G4yIjHMzD0KVpaCm.htm](bestiary-family-ability-glossary/G4yIjHMzD0KVpaCm.htm)|(Melfesh Monster) Burrowing Grasp|(Monstre melfesh) Poigne creusante|changé|
|[ICnpftxZEilrYjn0.htm](bestiary-family-ability-glossary/ICnpftxZEilrYjn0.htm)|(Werecreature) Curse of the Werecreature|(Créature garou) Malédiction du garou|changé|
|[iiIhLkjuPYJ93Upw.htm](bestiary-family-ability-glossary/iiIhLkjuPYJ93Upw.htm)|(Melfesh Monster) Smolderstench|(Monstre melfesh) Puanteur brûlante|changé|
|[IxJbFJ8dG5RbZWBD.htm](bestiary-family-ability-glossary/IxJbFJ8dG5RbZWBD.htm)|(Graveknight) Devastating Blast|(Chevalier sépulcre) Déflagration dévastatrice|changé|
|[ixPqVlqLaYTB1b23.htm](bestiary-family-ability-glossary/ixPqVlqLaYTB1b23.htm)|(Ghoul) Consume Flesh|(Goule) Dévorer la chair|changé|
|[jD0M3yV6gjkXafsJ.htm](bestiary-family-ability-glossary/jD0M3yV6gjkXafsJ.htm)|(Divine Warden) Divine Destruction|(Garde divin) Destruction divine|changé|
|[JSBmboE6bYVxDT9d.htm](bestiary-family-ability-glossary/JSBmboE6bYVxDT9d.htm)|(Ghost) Inhabit Object|(Fantôme) Occuper un objet|changé|
|[jTuK430yY8VgIByB.htm](bestiary-family-ability-glossary/jTuK430yY8VgIByB.htm)|(Ghost) Haunted House|(Fantôme) Maison hantée|changé|
|[KcUHCVhHnkMD8j3k.htm](bestiary-family-ability-glossary/KcUHCVhHnkMD8j3k.htm)|(Greater Barghest) Mutation - Toxic Breath|(Barghest supérieur) Mutation - Souffle toxique|changé|
|[kQZ6pzdSn6FaxWF2.htm](bestiary-family-ability-glossary/kQZ6pzdSn6FaxWF2.htm)|(Visitant) Visitant Spells|(Revenant) Sorts de revenant|changé|
|[m6teF5ADh7vuM8Zr.htm](bestiary-family-ability-glossary/m6teF5ADh7vuM8Zr.htm)|(Ghast) Consume Flesh|(Blême) Dévorer la chair|changé|
|[MhymIeTQoxbacG1o.htm](bestiary-family-ability-glossary/MhymIeTQoxbacG1o.htm)|(Zombie) Plague-Ridden|(Zombie) Pestiféré|changé|
|[mwEig0MYM7EIibSU.htm](bestiary-family-ability-glossary/mwEig0MYM7EIibSU.htm)|(Greater Barghest) Mutation - Vestigial Arm Strike|(Barghest supérieur) Mutation - Frappe de membre atrophié|changé|
|[N7UV5CZXtcoxDxCF.htm](bestiary-family-ability-glossary/N7UV5CZXtcoxDxCF.htm)|(Ghoul) Ghoul Fever|(Goule) Fièvre des goules|changé|
|[nF7RKPONY5H9kEIo.htm](bestiary-family-ability-glossary/nF7RKPONY5H9kEIo.htm)|(Ghost) Memento Mori|(Fantôme) Memento Mori|changé|
|[oXRnrQQ04oi8OkDG.htm](bestiary-family-ability-glossary/oXRnrQQ04oi8OkDG.htm)|(Vampire, Vrykolakas) Feral Possession|(Vampire, vrykolakas) Corruption sauvage|changé|
|[QlrbnkeZu6M4kvOy.htm](bestiary-family-ability-glossary/QlrbnkeZu6M4kvOy.htm)|(Worm That Walks) Discorporate|(Ver-qui-marche) Dématérialisation|changé|
|[qt2exWwQTzoObKfW.htm](bestiary-family-ability-glossary/qt2exWwQTzoObKfW.htm)|(Golem) Inexorable March|(Golem) Marche inexorable|changé|
|[s6JRMjgA7hSGUAYX.htm](bestiary-family-ability-glossary/s6JRMjgA7hSGUAYX.htm)|(Vampire, Vrykolakas Master) Pestilential Aura|(Vampire, maître vrykolakas) Aura pestilentielle|changé|
|[sajLbVE6VpCsR0Kl.htm](bestiary-family-ability-glossary/sajLbVE6VpCsR0Kl.htm)|(Skeleton) Bone Powder|(Squelette) Poudre d'os|changé|
|[SEzkqVJxr2eJDsuJ.htm](bestiary-family-ability-glossary/SEzkqVJxr2eJDsuJ.htm)|(Ghast) Stench|(Blême) Puanteur|changé|
|[tDfv2oEPal19NtSM.htm](bestiary-family-ability-glossary/tDfv2oEPal19NtSM.htm)|(Ghost) Fetch|(Fantôme) Double fantômatique|changé|
|[v6he7HLxYGzaMnvL.htm](bestiary-family-ability-glossary/v6he7HLxYGzaMnvL.htm)|(Zombie) Disgusting Pustules|(Zombie) Pustules écœurantes|changé|
|[VLCECgjkL5bNOxpx.htm](bestiary-family-ability-glossary/VLCECgjkL5bNOxpx.htm)|(Ravener) Consume Soul|(Dévoreur draconique) Dévorer les âmes|changé|
|[vOocS1EiRCXPtbgB.htm](bestiary-family-ability-glossary/vOocS1EiRCXPtbgB.htm)|(Visitant) Roar|(Revenant) Rugissement|changé|
|[VzKVJlX2ocv1ezzp.htm](bestiary-family-ability-glossary/VzKVJlX2ocv1ezzp.htm)|(Visitant) Noxious Breath|(Revenant) Souffle nocif|changé|
|[W77QhVNE46WDyMXi.htm](bestiary-family-ability-glossary/W77QhVNE46WDyMXi.htm)|(Skeleton) Explosive Death|(Squelette) Mort explosive|changé|
|[wh2T2L5SMsa32RyE.htm](bestiary-family-ability-glossary/wh2T2L5SMsa32RyE.htm)|(Lich) Cold Beyond Cold|(Liche) Au-delà du froid|changé|
|[wqptDG0IW6ExnISC.htm](bestiary-family-ability-glossary/wqptDG0IW6ExnISC.htm)|(Vampire, True) Create Spawn|(Vampire, véritable) Création de rejetons|changé|
|[XbHMVjHtbPaPr9P5.htm](bestiary-family-ability-glossary/XbHMVjHtbPaPr9P5.htm)|(Ravener) Vicious Criticals|(Dévoreur draconique) Critiques vicieux|changé|
|[XHut4MN0JBgm7WaN.htm](bestiary-family-ability-glossary/XHut4MN0JBgm7WaN.htm)|(Vampire, True) Drink Blood|(Vampire, véritable) Boire le sang|changé|
|[xS8ybzuqPSi3Jb8k.htm](bestiary-family-ability-glossary/xS8ybzuqPSi3Jb8k.htm)|(Clockwork Creature) Wind-Up|(Créature mécanique) Remonter|changé|
|[XvPm866TKSfclErJ.htm](bestiary-family-ability-glossary/XvPm866TKSfclErJ.htm)|(Lich) Paralyzing Touch|(Liche) Toucher paralysant|changé|
|[yWNcEsEJIoeXKBnk.htm](bestiary-family-ability-glossary/yWNcEsEJIoeXKBnk.htm)|(Lich) Void Shroud|(Liche) Linceul de néant|changé|
|[Zu6feO9NUZJlsKuc.htm](bestiary-family-ability-glossary/Zu6feO9NUZJlsKuc.htm)|(Worm That Walks) Squirming Embrace|(Ver-qui-marche) Etreinte grouillante|changé|

## Liste des traductions complétés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[08egiRxOvMX97XTc.htm](bestiary-family-ability-glossary/08egiRxOvMX97XTc.htm)|(Werecreature) Change Shape|(Créature garou) Changement de forme|officielle|
|[0B9eEetIbeSPhulD.htm](bestiary-family-ability-glossary/0B9eEetIbeSPhulD.htm)|(Divine Warden) Divine Domain Spells|(Garde divin) Sorts divins de domaine|libre|
|[0K3vhX14UEPftqu8.htm](bestiary-family-ability-glossary/0K3vhX14UEPftqu8.htm)|(Ghost) Telekinetic Assault|(Fantôme) Assaut télékinésique|officielle|
|[4Aj4FWKObfMYoSvh.htm](bestiary-family-ability-glossary/4Aj4FWKObfMYoSvh.htm)|(Ghost) Beatific Appearance|(Fantôme) Apparence bienheureuse|libre|
|[4p06ewIsPnWZwuwc.htm](bestiary-family-ability-glossary/4p06ewIsPnWZwuwc.htm)|(Ghost) Lynchpin|(Fantôme) Pilier|libre|
|[4UEZY3G3CiCP78Kx.htm](bestiary-family-ability-glossary/4UEZY3G3CiCP78Kx.htm)|(Visitant) Wrestle|(Revenant) Lutte|officielle|
|[5arYoSY5kcRo8TeM.htm](bestiary-family-ability-glossary/5arYoSY5kcRo8TeM.htm)|(Vampire, Vrykolakas) Vrykolakas Vulnerabilities|(Vampire, Vrykolakas) Vulnérabilités des vrykolakas|officielle|
|[5dbzWZbiTyPKgwKS.htm](bestiary-family-ability-glossary/5dbzWZbiTyPKgwKS.htm)|(Graveknight) Graveknight's Curse|(Chevalier sépulcre) Malédiction du chevalier sépulcre|officielle|
|[5DuBTf37u88IrphJ.htm](bestiary-family-ability-glossary/5DuBTf37u88IrphJ.htm)|(Vampire, Basic) Vampire Weaknesses|(Vampire, de base) Faiblesses du vampire|officielle|
|[5lv9r2ubDCov4dFn.htm](bestiary-family-ability-glossary/5lv9r2ubDCov4dFn.htm)|(Ghost) Pyre's Memory|(Fantôme) Mémoire de bûcher funéraire|libre|
|[6Alm4mLj3ORxCXC2.htm](bestiary-family-ability-glossary/6Alm4mLj3ORxCXC2.htm)|(Ravener) Cowering Fear|(Dévoreur draconique) Recroquevillé de terreur|officielle|
|[6isn9nqnvfRrC1wW.htm](bestiary-family-ability-glossary/6isn9nqnvfRrC1wW.htm)|(Graveknight) Weapon Master|(Chevalier sépulcre) Maître d'armes|officielle|
|[7jWANvnDmNcQvFve.htm](bestiary-family-ability-glossary/7jWANvnDmNcQvFve.htm)|(Graveknight) Ruinous Weapons|(Chevalier sépulcre) Armes du désastre|officielle|
|[871jo3ZGnybF6dC8.htm](bestiary-family-ability-glossary/871jo3ZGnybF6dC8.htm)|(Lich) Blasphemous Utterances|(Liche) Paroles blasphématoires|officielle|
|[98twzz56mMGN5Ftw.htm](bestiary-family-ability-glossary/98twzz56mMGN5Ftw.htm)|(Vampire, Vrykolakas) Drink Blood|(Vampire, vrykolakas) Boire le sang|officielle|
|[9oy2zWxy7Klk9FxR.htm](bestiary-family-ability-glossary/9oy2zWxy7Klk9FxR.htm)|(Skeleton) Blaze|(Squelette) Embrasement|libre|
|[9wX6EFimVFYFZjD7.htm](bestiary-family-ability-glossary/9wX6EFimVFYFZjD7.htm)|(Ulgrem-Axaan) Ichor Coating|(Ulgrem-Axaan) Couvert d'ichor|libre|
|[a7A4xY3NeWI2Ya4Z.htm](bestiary-family-ability-glossary/a7A4xY3NeWI2Ya4Z.htm)|(Lich) Rejuvenation|(Liche) Reconstruction|officielle|
|[aiwlPSmNY9b6Psvd.htm](bestiary-family-ability-glossary/aiwlPSmNY9b6Psvd.htm)|(Ghost) Frightful Moan|(Fantôme) Lamentation d'épouvante|officielle|
|[AjTkksppymqzCivT.htm](bestiary-family-ability-glossary/AjTkksppymqzCivT.htm)|(Vampire, True) Dominate|(Vampire, véritable) Domination|officielle|
|[awFTdoQfYva84HkU.htm](bestiary-family-ability-glossary/awFTdoQfYva84HkU.htm)|(Ulgrem-Axaan) Befouling Odor|(Ulgrem-Axaan) Odeur de souillure|libre|
|[AydIHjDNbOZizj5U.htm](bestiary-family-ability-glossary/AydIHjDNbOZizj5U.htm)|(Ghost) Cold Spot|(Fantôme) Point froid|libre|
|[b3p8x6sgTa0BOvAb.htm](bestiary-family-ability-glossary/b3p8x6sgTa0BOvAb.htm)|(Lich) Siphon Life|(Liche) Siphonner la vie|officielle|
|[BcSlVpaN72LoQ5BV.htm](bestiary-family-ability-glossary/BcSlVpaN72LoQ5BV.htm)|(Ghost) Site Bound|(Fantôme) Lié à un site|officielle|
|[BgbSHRdkGH7raOgA.htm](bestiary-family-ability-glossary/BgbSHRdkGH7raOgA.htm)|(Graveknight) Phantom Mount|(Chevalier sépulcre) Monture fantôme|officielle|
|[br5Oup4USIUXQani.htm](bestiary-family-ability-glossary/br5Oup4USIUXQani.htm)|(Vampire, Vrykolakas) Swift Tracker|(Vampire, vrykolakas) Pistage accéléré|officielle|
|[BSLqIYqxcCVBb2Vp.htm](bestiary-family-ability-glossary/BSLqIYqxcCVBb2Vp.htm)|(Zombie) Unkillable|(Zombie) Intuable|officielle|
|[bTJnxBQjr7G8yr30.htm](bestiary-family-ability-glossary/bTJnxBQjr7G8yr30.htm)|(Graveknight) Sacrilegious Aura|(Chevalier sépulcre) Aura sacrilège|officielle|
|[bVZ6KizWVTLJUBXi.htm](bestiary-family-ability-glossary/bVZ6KizWVTLJUBXi.htm)|(Ghast) Paralysis|(Blême) Paralysie|officielle|
|[c04ICnrzygyFG3PK.htm](bestiary-family-ability-glossary/c04ICnrzygyFG3PK.htm)|(Vampire, Basic) Drink Blood|(Vampire, de base) Boire le sang|officielle|
|[CxiEpXt7Gw3tSIOh.htm](bestiary-family-ability-glossary/CxiEpXt7Gw3tSIOh.htm)|(Zombie) Rotting Aura|(Zombie) Aura de pourrissement|officielle|
|[cYkEpJzpMu3mCrFc.htm](bestiary-family-ability-glossary/cYkEpJzpMu3mCrFc.htm)|(Ghost) Phantasmagoria|(Fantôme) Phantasmagorie|libre|
|[DlHTe9jLssEELY6a.htm](bestiary-family-ability-glossary/DlHTe9jLssEELY6a.htm)|(Divine Warden) Divine Innate Spells|(Garde divin) Sorts divins innés|libre|
|[EM6YZCt6vFCFhXmb.htm](bestiary-family-ability-glossary/EM6YZCt6vFCFhXmb.htm)|(Goblin) Goblin Scuttle|(Gobelin) Précipitation gobeline|officielle|
|[eSp48kG7v1GNuGgh.htm](bestiary-family-ability-glossary/eSp48kG7v1GNuGgh.htm)|(Visitant) Vengeful Presence|(Revenant) Présence vengeresse|officielle|
|[eXleBXdAemiEoHA8.htm](bestiary-family-ability-glossary/eXleBXdAemiEoHA8.htm)|(Nymph Queen) Change Shape|(Reine Nymphe) Changement de forme|libre|
|[FA0ri2fAcMa1HgZe.htm](bestiary-family-ability-glossary/FA0ri2fAcMa1HgZe.htm)|(Werecreature) Moon Frenzy|(Créature garou) Frénésie lunaire|officielle|
|[fmUBaLklyVmNt3VD.htm](bestiary-family-ability-glossary/fmUBaLklyVmNt3VD.htm)|(Graveknight) Clutching Armor|(Chevalier sépulcre) Armure agrippante|libre|
|[fqZhojt2M5LfSKSH.htm](bestiary-family-ability-glossary/fqZhojt2M5LfSKSH.htm)|(Lich) Drain Soul Cage|(Liche) Canaliser le phylactère|officielle|
|[FsqVhavMWAoFro1L.htm](bestiary-family-ability-glossary/FsqVhavMWAoFro1L.htm)|(Ravener) Soul Ward|(Dévoreur draconique) Armure d’âmes|officielle|
|[fTk5nmm7HHtXOdSo.htm](bestiary-family-ability-glossary/fTk5nmm7HHtXOdSo.htm)|(Lich) Dark Deliverance|(Liche) Sombre salut|officielle|
|[fVyoHEO3fSR737M1.htm](bestiary-family-ability-glossary/fVyoHEO3fSR737M1.htm)|(Ghost) Draining Touch|(Fantôme) Toucher affaiblissant|officielle|
|[FW4KAUHb7r8WkxUc.htm](bestiary-family-ability-glossary/FW4KAUHb7r8WkxUc.htm)|(Ghoul) Paralysis|(Goule) Paralysie|officielle|
|[FXHjmH1oce7Z3tZb.htm](bestiary-family-ability-glossary/FXHjmH1oce7Z3tZb.htm)|(Vampire, Basic) Coffin Restoration|(Vampire, de base) Cercueil restorateur|officielle|
|[fYDrunTldWmFvfjl.htm](bestiary-family-ability-glossary/fYDrunTldWmFvfjl.htm)|(Ravener) Soulsense|(Dévoreur draconique) Perception de l’âme|officielle|
|[ga0Oj7mmjSwWQgmR.htm](bestiary-family-ability-glossary/ga0Oj7mmjSwWQgmR.htm)|(Ghost) Corrupting Gaze|(Fantôme) Regard corrupteur|officielle|
|[hA6HsM4i4yPfEsDH.htm](bestiary-family-ability-glossary/hA6HsM4i4yPfEsDH.htm)|(Ghast) Ghast Fever|(Blême) Fièvre des blêmes|officielle|
|[haMVGGDY3mLuKy9s.htm](bestiary-family-ability-glossary/haMVGGDY3mLuKy9s.htm)|(Vampire, Vrykolakas Master) Bubonic Plague|(Vampire, maître vrykolakas) Peste bubonique|officielle|
|[HdSaMUb4uEsbtQTn.htm](bestiary-family-ability-glossary/HdSaMUb4uEsbtQTn.htm)|(Skeleton) Skeleton of Roses|(Squelette) Squelette de roses|libre|
|[hEWAzlIoJg3NzA7Q.htm](bestiary-family-ability-glossary/hEWAzlIoJg3NzA7Q.htm)|(Spring-Heeled Jack) Change Shape|(Jack à ressort) Changement de forme|libre|
|[HKFoOZZV4WdjkeeJ.htm](bestiary-family-ability-glossary/HKFoOZZV4WdjkeeJ.htm)|(Protean) Warpwave|(Protéen) Vagues de distorsion|libre|
|[HLPLYJ9bOazb2ZPX.htm](bestiary-family-ability-glossary/HLPLYJ9bOazb2ZPX.htm)|(Ravener) Discorporate|(Dévoreur draconique) Dématérialisation|officielle|
|[i4WBOAb7CmY53doM.htm](bestiary-family-ability-glossary/i4WBOAb7CmY53doM.htm)|(Melfesh Monster) False Synapses|(Monstre melfesh) Faux synapses|libre|
|[i7m74TphAiFYvzPL.htm](bestiary-family-ability-glossary/i7m74TphAiFYvzPL.htm)|(Ghost) Ghost Storm|(Fantôme) Tempête Fantôme|libre|
|[iAXHLkxuuCUOwqkN.htm](bestiary-family-ability-glossary/iAXHLkxuuCUOwqkN.htm)|(Werecreature) Animal Empathy|(Créature garou) Empathie animale|officielle|
|[IFQd4GiHlV33p7oK.htm](bestiary-family-ability-glossary/IFQd4GiHlV33p7oK.htm)|(Nymph Queen) Inspiration|(Reine nymphe) Inspiration|libre|
|[IOk0MRs3f9FrarKL.htm](bestiary-family-ability-glossary/IOk0MRs3f9FrarKL.htm)|(Protean) Protean Anatomy|Anatomie protéenne|libre|
|[ipGBYIXk4u47Mp1D.htm](bestiary-family-ability-glossary/ipGBYIXk4u47Mp1D.htm)|(Zombie) Feast|(Zombie) Festoyer|officielle|
|[iwLj14liESK5OBN8.htm](bestiary-family-ability-glossary/iwLj14liESK5OBN8.htm)|(Ghost) Malevolent Possession|(Fantôme) Possession malveillante|officielle|
|[jjWisLWwcdxsqv8o.htm](bestiary-family-ability-glossary/jjWisLWwcdxsqv8o.htm)|(Kallas Devil) Blameless|(Diablesse Kallas) Irréprochable|libre|
|[K5bNE90vmeFzktnM.htm](bestiary-family-ability-glossary/K5bNE90vmeFzktnM.htm)|(Skeleton) Collapse|(Squelette) Écroulement|officielle|
|[kG4fDd16fYEFvmgy.htm](bestiary-family-ability-glossary/kG4fDd16fYEFvmgy.htm)|(Nymph Queen) Nymph's Beauty|(Reine nymphe) Beauté de la reine|libre|
|[KJrQdJQ0ZEmKYH4Y.htm](bestiary-family-ability-glossary/KJrQdJQ0ZEmKYH4Y.htm)|(Kallas Devil) Suffer the Children|(Diablesse kallas) Subir les enfants|libre|
|[KwmYKsaXHKfZgB2c.htm](bestiary-family-ability-glossary/KwmYKsaXHKfZgB2c.htm)|(Spring-Heeled Jack) Vanishing Leap|(Jack à ressort) Saut évanouissant|libre|
|[L80gn6WOWi9roJW3.htm](bestiary-family-ability-glossary/L80gn6WOWi9roJW3.htm)|(Divine Warden) Instrument of Faith|(Garde divin) Instrument de foi|libre|
|[LOd8QwaKP3hnyGkc.htm](bestiary-family-ability-glossary/LOd8QwaKP3hnyGkc.htm)|(Kallas Devil) Cold Currents|(diablesse Kallas) Courants froids|libre|
|[MAC97gjFcdiqLyhp.htm](bestiary-family-ability-glossary/MAC97gjFcdiqLyhp.htm)|(Skeleton) Screaming Skull|(Squelette) Crâne hurlant|officielle|
|[mEbrInCpag7YThH2.htm](bestiary-family-ability-glossary/mEbrInCpag7YThH2.htm)|(Dragon) Change Shape|(Dragon) Changement de forme|officielle|
|[mZpX54PgTxPta5y4.htm](bestiary-family-ability-glossary/mZpX54PgTxPta5y4.htm)|(Graveknight) Graveknight's Shield|(Chevalier sépulcre) Bouclier du chevalier sépulcre|libre|
|[na1WJDEaoqpcQuOR.htm](bestiary-family-ability-glossary/na1WJDEaoqpcQuOR.htm)|(Kallas Devil) Infectious Water|(Diablesse Kallas) Eau infectieuse|libre|
|[Nnl5wg6smOzieTop.htm](bestiary-family-ability-glossary/Nnl5wg6smOzieTop.htm)|(Vampire, True) Turn to Mist|(Vampire, véritable) Se changer en brume|officielle|
|[noOXyIXmwYN2rRd1.htm](bestiary-family-ability-glossary/noOXyIXmwYN2rRd1.htm)|(Vampire, True) Children of the Night|(Vampire, véritable) Enfant de la nuit|officielle|
|[NSO2l1jXK32oTnVP.htm](bestiary-family-ability-glossary/NSO2l1jXK32oTnVP.htm)|(Kallas Devil)  Freezing Touch|(Diablesse Kallas) Contact frigorifiant|libre|
|[oD1cA9uXYQ8evT8f.htm](bestiary-family-ability-glossary/oD1cA9uXYQ8evT8f.htm)|(Melfesh Monster) Vent Flames|(Monstre Melfesh) Évents de fammes|libre|
|[OD3VPUalSKDTYFmF.htm](bestiary-family-ability-glossary/OD3VPUalSKDTYFmF.htm)|(Greater Barghest) Mutation - Wings|(Barghest supérieur) Mutation - Ailes|officielle|
|[OMSsLUcnRj6ycEUa.htm](bestiary-family-ability-glossary/OMSsLUcnRj6ycEUa.htm)|(Graveknight) Channel Magic|(Chevalier sépulcre) Canaliser la magie|libre|
|[OPGu0WsUHpHSmu80.htm](bestiary-family-ability-glossary/OPGu0WsUHpHSmu80.htm)|(Divine Warden) Faith Bound|(Garde divin) Liens de foi|libre|
|[P5YTG6I8ci4lwhZ1.htm](bestiary-family-ability-glossary/P5YTG6I8ci4lwhZ1.htm)|(Skeleton) Bloody|(Squellette) Sanglant|officielle|
|[pEJkjqjjBXj4YjYZ.htm](bestiary-family-ability-glossary/pEJkjqjjBXj4YjYZ.htm)|(Kuworsys) Careless Block|(Kuworsys) Blocage insouciant|libre|
|[PjYwPIUUrirQFiee.htm](bestiary-family-ability-glossary/PjYwPIUUrirQFiee.htm)|(Kallas Devil) Underwater Views|(Diablesse kallas) Vues sous marines|libre|
|[pxiSbjfWaKCG1xLD.htm](bestiary-family-ability-glossary/pxiSbjfWaKCG1xLD.htm)|(Graveknight) Betrayed Revivification|(Chevalier sépulcre) Résurrection vengeresse|officielle|
|[qlNOqsfJH2gj7DCs.htm](bestiary-family-ability-glossary/qlNOqsfJH2gj7DCs.htm)|(Kallas Devil) Waterfall Torrent|(Diablesse Kallas) Cascade torrentielle|libre|
|[QMWvYf8Qowj2Fzmx.htm](bestiary-family-ability-glossary/QMWvYf8Qowj2Fzmx.htm)|(Graveknight) Dark Deliverance|(Chevalier sépulcre) Sombre salut|officielle|
|[qs21GajPmFYel3pc.htm](bestiary-family-ability-glossary/qs21GajPmFYel3pc.htm)|(Melfesh Monster) Corpse Bomb|(Monstre Melfesh) Bombe cadavérique|libre|
|[QuHeuVNDnmOCS9M1.htm](bestiary-family-ability-glossary/QuHeuVNDnmOCS9M1.htm)|(Vampire, Vrykolakas Master) Create Spawn|(Vampire, maître vrykolakas) Création de rejetons|officielle|
|[r34QDwKiWZoVymJa.htm](bestiary-family-ability-glossary/r34QDwKiWZoVymJa.htm)|(Golem) Golem Antimagic|(Golem) Antimagie des golems|officielle|
|[RgYpzTk3XapgVeXZ.htm](bestiary-family-ability-glossary/RgYpzTk3XapgVeXZ.htm)|(Skeleton) Bone Missile|(Squelette) Côte missile|libre|
|[RQ9NaIerG95wkhjl.htm](bestiary-family-ability-glossary/RQ9NaIerG95wkhjl.htm)|(Ulgrem-Axaan) Fallen Victim|(Ulgrem-Axaan) Victime déchue|libre|
|[RvzFTym3r22VhMrm.htm](bestiary-family-ability-glossary/RvzFTym3r22VhMrm.htm)|(Kuworsys) Smash and Grab|(Kuworsys) Frapper et saisir|libre|
|[sAyR4uJbsWukZFZf.htm](bestiary-family-ability-glossary/sAyR4uJbsWukZFZf.htm)|(Vampire, True) Mist Escape|(Vampire, véritable) Fuite brumeuse|officielle|
|[sECjzfaYaW68vbLV.htm](bestiary-family-ability-glossary/sECjzfaYaW68vbLV.htm)|(Vampire, Vrykolakas Master) Change Shape|(Vampire, maître vrykolakas) Changement de forme|officielle|
|[SEmSk1INZDmeoB5R.htm](bestiary-family-ability-glossary/SEmSk1INZDmeoB5R.htm)|(Nymph Queen) Focus Beauty|(Reine nymphe) Focaliser la beauté|libre|
|[tKixfds07IRH2nnh.htm](bestiary-family-ability-glossary/tKixfds07IRH2nnh.htm)|(Vampire, Vrykolakas Master) Children of the Night|(Vampire, maître vrykolakas) Enfants de la nuit|officielle|
|[tQY2xmcIhzCM7oCC.htm](bestiary-family-ability-glossary/tQY2xmcIhzCM7oCC.htm)|(Melfesh Monster) Torrential Advance|(Monstre melfesh) Avancée torrentielle|libre|
|[UHuA1hue5xrRM6KK.htm](bestiary-family-ability-glossary/UHuA1hue5xrRM6KK.htm)|(Nymph Queen) Tied to the Land|(Reine nymphe) Liée au territoire|libre|
|[UlKGxPaKtRGWuq3V.htm](bestiary-family-ability-glossary/UlKGxPaKtRGWuq3V.htm)|(Divine Warden) Faithful Weapon|(Garde divin) Arme fidèle|libre|
|[unR8VVR4yyRnsmnB.htm](bestiary-family-ability-glossary/unR8VVR4yyRnsmnB.htm)|(Ghost) Rejuvenation|(Fantôme) Reconstruction|officielle|
|[UOqq8lkBPcPtKoCN.htm](bestiary-family-ability-glossary/UOqq8lkBPcPtKoCN.htm)|(Melfesh Monster) Flame Lash|(Monstre melfesh) Lâcher de flammes|libre|
|[UU1Fp3PRuTFONjC9.htm](bestiary-family-ability-glossary/UU1Fp3PRuTFONjC9.htm)|(Ghoul) Swift Leap|(Goule) Bond rapide|officielle|
|[uV4jf2pkMRGLdhJX.htm](bestiary-family-ability-glossary/uV4jf2pkMRGLdhJX.htm)|(Ghost) Dreamwalker|(Fantôme) Marcheur de rêves|libre|
|[VXKiqiW7gXfFCz1U.htm](bestiary-family-ability-glossary/VXKiqiW7gXfFCz1U.htm)|(Melfesh Monster) Mycelial Tomb|(Monstre melfesh) Tombe mycélienne|libre|
|[W5fD1ebH6Ri8HNzh.htm](bestiary-family-ability-glossary/W5fD1ebH6Ri8HNzh.htm)|(Graveknight) Create Grave Squire|(Chevalier sépulcre) Grand écuyer du sépulcre|officielle|
|[wdO3EvDCGoDYW44S.htm](bestiary-family-ability-glossary/wdO3EvDCGoDYW44S.htm)|(Melfesh Monster) Death Throes|(Monstre Melfesh) Affres de la mort|libre|
|[wlhvSroB6r5cSd8Y.htm](bestiary-family-ability-glossary/wlhvSroB6r5cSd8Y.htm)|(Greater Barghest) Mutation - Poison Fangs|(Barghest supérieur) Mutation - Crocs poison|officielle|
|[WQjFPBQPDu6vyJny.htm](bestiary-family-ability-glossary/WQjFPBQPDu6vyJny.htm)|(Ghost) Corporeal Manifestation|(Fantôme) Manifestation corporelle|libre|
|[WtRxZEPH963RkUCj.htm](bestiary-family-ability-glossary/WtRxZEPH963RkUCj.htm)|(Kuworsys) Rain Blows|(Kuworsys) Pluie de coups|libre|
|[wzezkgOufQev7BLK.htm](bestiary-family-ability-glossary/wzezkgOufQev7BLK.htm)|(Ghost) Revenant|(Fantôme) Revenant|libre|
|[X2mdpb1Dhl890YbA.htm](bestiary-family-ability-glossary/X2mdpb1Dhl890YbA.htm)|(Worm That Walks) Swarm Shape|(Ver-qui-marche) Forme de nuée|officielle|
|[Y46uZK6X7J7iMsgq.htm](bestiary-family-ability-glossary/Y46uZK6X7J7iMsgq.htm)|(Ulgrem-Axaan) Crocodile Tears|(Ulgreem--Aaxan) Larmes de crocodile|libre|
|[yaDe9hO9fm2N4SqH.htm](bestiary-family-ability-glossary/yaDe9hO9fm2N4SqH.htm)|(Spring-Heeled Jack) Resonant Terror|(Jack à ressort) Terreur résonnante|libre|
|[yfcqbMRmCkfPWV9O.htm](bestiary-family-ability-glossary/yfcqbMRmCkfPWV9O.htm)|(Ulgrem-Axaan) Crushing Weight|(Ulgrem-Axaan) Poids écrasant|libre|
|[ZIFaA4jDQjM0vq8q.htm](bestiary-family-ability-glossary/ZIFaA4jDQjM0vq8q.htm)|(Graveknight) Rejuvenation|(Chevalier sépulcre) Reconstruction|officielle|
|[ZMTnwyXjyfy7Ryi6.htm](bestiary-family-ability-glossary/ZMTnwyXjyfy7Ryi6.htm)|(Vampire, True) Change Shape|(Vampire, véritable) Changement de forme|officielle|
|[zwgUxJBNFqWOuaBX.htm](bestiary-family-ability-glossary/zwgUxJBNFqWOuaBX.htm)|(Vampire, Vrykolakas Master) Dominate Animal|(Vampire, maître vrykolakas) Domination animale|officielle|
